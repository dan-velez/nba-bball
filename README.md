# nba-games
List NBA games for the current date on the CLI or as a JSON API.

## Installation
`npm i nba-games`

## Usage
### CLI
```
$ nba-games [list <day | week> | score <game>]

# Examples
$ nba-games list day

$ nba-games list week

Chicago CHI @ Memphis MEM
1:00 AM UTC
Wednesday, February 27
___________________________________

Portland POR @ Boston BOS
1:00 AM UTC on http://www.espn.com/watch/
Wednesday, February 27
___________________________________

Detroit DET @ San Antonio SA
1:30 AM UTC
Wednesday, February 27
___________________________________

# Note these do not work yet, will be implemented.
$ nba-games score OKC

$ nba-games score all
```

### API
#### `nba_games.list([opts])`
```
nba_games.nba_games_day().then(games => {
  console.log(JSON.stringify(games, null, 2));
});
[
  {
    "home": "MIA",
    "away": "BOS",
    "time": "7:30 pm EST",
    "network": null
  },
  {
    "home": "OKC",
    "away": "GS",
    "time": "8:00 pm EST",
    "network": "ESPN"
  }
]
```
