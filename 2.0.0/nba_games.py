#!/usr/bin/python3
# nba_games.py - Lookup schedule for NBA basketball games.

import  bs4
import json
from selenium import webdriver

def rendfetch(url):
    browser = webdriver.PhantomJS()
    browser.get(url)
    html = browser.page_source
    browser.close()
    return html

def list_games_day():
    return

def list_games_week():
    results = [] # All games for the week.
    url = "http://www.espn.com/nba/schedule"
    html = rendfetch(url)
    soup = bs4.BeautifulSoup(html, 'html.parser')
    tables = soup.select('table')
    days = soup.select('h2.table-caption')
    for i in range(0, len(days)-1):
        day = days[i].string
        if day:
            print('NBA Games for', day)
            for tr in tables[i].select('tr'):
                i = 0
                game = {}
                for td in tr.select('td'):
                    text = td.get_text()
                    if i == 0:
                        game['away'] = text
                    elif i == 1:
                        game['home'] = text
                    elif i == 2:
                        game['time'] = text
                    elif i == 3:
                        game['tv'] = text
                    i += 1
                print(game)

list_games_week()
# html = rendfetch("http://www.espn.com/nba/schedule")
# print(html)
