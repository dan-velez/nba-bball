/* nba_games.js - Lookup NBA basketball schedule for the day. */
/* TODO
 * Turn to rest api online
 */
const Browser = require('zombie');
const cheerio = require('cheerio');
const url = "http://www.espn.com/nba/schedule";
const num_fields = 4;
const phantom = require('phantom');

module.exports = {
    nba_games_day,
    nba_games_week
};

async function main() {
    /* Request the schedule webpage and return a JSON object containing
     * all the games for the week.
     */
	let results = [];
    let html = await rendfetch(url);
    console.log("Response size: " + Math.round((html.length / 1024)) + " kb");
    let $ = cheerio.load(html);
    let table = $("table.schedule");
    let table_dates = [];
    let date_counter = 0;
    let current_row_time = null; /* Determine when the table is on the next date. */
    let current_row_date = null;
    let previous_row_time = null;
    /* Parse html for class='table-caption' */
    $('body').find('h2.table-caption').each(function(i, e) {
        let tdate = $(this).text();
        table_dates.push(tdate);
    });
    // console.log("Table dates parsed", table_dates);
    current_row_date = table_dates[0];
    /* Parse each game row. */
    table.find('tr').each(function(i, e) {
        let game = {}; 
        $(this).find('td').each(function(j, ee) {
            /* Parse the row by index. */
            if(j == 0) {
                /* AWAY */
                game.away = $(this).text();
            }
            else if(j == 1) {
                /* HOME */
                game.home = $(this).text();
            }
            else if(j == 2) {
                /* TIME & DATE
                 * For each row, if time is less then previous,
                 * go to next date.
                 * ADDTIMEZONE (System time);
                 */
                let timezone = new Date().getTimezoneOffset();
                game.time = $(this).text();
                if(previous_row_time == null) {
                    previous_row_time = game.time;
                    if(previous_row_time == "LIVE") {
                        previous_row_time = "12:00 AM";
                    }
                }
                if(time_cmp(game.time, previous_row_time) == 2) {
                    try {
                        current_row_date = table_dates[++date_counter];
                    } catch(err) {

                    }
                }
                if((game.time != "LIVE") && (game.time != null)) {
                    previous_row_time = game.time;
                }
                game.date = current_row_date;
            }
            /* NETWORK */
            else if(j == 3) {
                game.network = $(this).html()
                if(game.network) {
                    let $$ = cheerio.load(game.network);
                    let href = $$('a').attr('href') || $(this).text();
                    // console.log('found network:', href);
                    game.network = href;
                }
            }
        });
        if(game.home != undefined) results.push(game);
        /*
        if(Object.keys(game).length == num_fields) {
        }*/
    });
    // console.log(JSON.stringify(results, null, 2));
    return results;
}

async function rendfetch(url) {
    /* Request a webpage and return the Javascript rendered result. */
    console.log("Requesting data...");
    const instance = await phantom.create();
    const page = await instance.createPage();
    const status = await page.open(url);
    const content = await page.property('content');
    // const headers = await page.property('headers');
    await instance.exit();
    return content;
}

function nba_games_day(day) {
    /* Return a JSON object containing the games for the day. */
    return new Promise((resolve, reject)=> {
        let schedule_pr = main();
        schedule_pr.then((resp)=> {
            /* Get only the games for the day. */
            let games = [];
            resp.map((game)=> {
                /* Parse the date. */
                let parsed_date = new Date(game.date);
                let today = new Date();
                /* Push if the date is todays. */
                if(
                    (parsed_date.getDate().toString()+
                    parsed_date.getMonth().toString()+
                    parsed_date.getFullYear().toString()) ==

                    (today.getDate().toString()+
                    today.getMonth().toString()+
                    today.getFullYear().toString())
                ) {
                    games.push(game);
                }
            });
            resolve(games);
        });
    });
}

function nba_games_week() {
    /* Return a JSON object containing the games for the week. */
    return new Promise((resolve, reject)=> {
        let schedule_pr = main();
        schedule_pr.then((resp)=> {
            resolve(resp);
        });
    });
}
/*
nba_games_week().then(resp => {
    console.log("NBA Games for the day");
    console.log(resp);
});
*/

function time_cmp(time1, time2) {
    /* Compare 2 times of current day. Returns 1, 2, or 0. 
     * input format: HH:MM AM/PM
     * */
    if(!time1 || !time2) {
        console.log(`Cannot time_cmp [${time1}] and [${time2}]`);
        return null;
    }
    time1 = convertTime12to24(time1);
    time2 = convertTime12to24(time2);
    let time1_date = new Date("01 Jan 1970 " + time1+":00");
    let time2_date = new Date("01 Jan 1970 " + time2+":00");
    if(time1_date.getTime() > time2_date.getTime()) return 1;
    else if(time1_date.getTime() < time2_date.getTime()) return 2;
    else return 0;
}

function convertTime12to24(time12h) {
    /* Convert time in format 00:00 MM to 24 hour. */
    const [time, modifier] = time12h.split(' ');
    let [hours, minutes] = time.split(':');
    if (hours === '12') {
        hours = '00';
    }
    if (modifier === 'PM') {
        hours = parseInt(hours, 10) + 12;
    }
    return hours + ':' + minutes;
}

/*
console.log(
convertTime12to24("1:00 PM"))
console.log(time_cmp(
    "12:30 AM",
    "1:00 AM"
))
*/
